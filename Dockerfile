FROM python:latest

ENV PYTHONUNBUFFERED=1
wORKDIR /usr/scr
COPY requirements.txt .
RUN /usr/local/bin/python -m pip install --upgrade pip
RUN pip install -r requirements.txt

COPY . .

